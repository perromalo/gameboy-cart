# GameBoy Cart FPGA Emulation

Proyecto de emular un cartucho de gameboy a traves de una fpga utilizando herramientas libres.<br>
Casi todo el trabajo se realizara en [kicad](http://kicad-pcb.org/),[icestudio](http://kicad-pcb.org/) y Linux.

Por que empiezo esta batalla?

A la hora de pasar lo que tengamos programado a cartucho fisico, podemos o comprar un flash cart, o realizar nuestro propio cartucho para la gameboy.

Ventajas del flash cart: viene echo. Desventajas: caro y limitado.

Ventajas de fabricar el cartucho: barato? y opciones ilimitadas. Desventajas: Requiere tiempo y herramientas que si no las tenemos encarecen el proyecto.

en esta ultima opcion si queremos tener un cartucho de mas de 32Kb necesitaremos un mapper,<br>
de donde consigo un mapper? los mappers se pueden conseguir de "cartuchos donantes".

pienso que en el infierno ahi un sitio especial reservado para los que rompen cartuchos de videojuegos : <br>
asi que tendremos que emular un mapper en una fpga para poder desarrollar y experimentar con nuestra preciosa maquina :)

Probaremos la paca de desarrollo de lattice [iCE40HX8KB](http://www.latticesemi.com/en/Products/DevelopmentBoardsAndKits/iCE40HX8KBreakoutBoard.aspx) 
utilizando open software para programar y sintetizar codigo gracias al grandisimo trabajo de: 
[icestudio](https://github.com/FPGAwars/icestudio), [Icestorm project](http://www.clifford.at/icestorm/) y [Apio](https://github.com/FPGAwars/apio) podeis encontrar el manual de instalacion de icestudio en este [link](https://github.com/FPGAwars/icestudio#installation).

Lo primero es leer mucho los putos manuales RTFM :)

Pandocs documento  con mucha info sobre gameboy [Everything You Always Wanted To Know About GAMEBOY](http://gbdev.gg8.se/files/docs/mirrors/pandocs.html#aboutthepandocs)<br>
Game Boy Development community [gbdev.github.io](https://gbdev.github.io/)<br>

Despues de mucho pensar y teniendo las ideas un poco mas claras, nos ponemos manos a la obra.<br>
Como dijo jack el destripador, vayamos por partes...

lo primero es conectar la gameboy a la fpga, para poder empezar a experimentar,<br>
para ello tenemos que convertir el voltaje del conector de cartuchos que es de 5v a el voltage utilizado por la placa de desarrollo fpga q son 3,3 volts.<br>
Asi que empieza la primera parte...

## Game boy cart 5v to 3,3v translation PCB

Despues de descargar el pinout del cartucho de [old.pinouts.ru](http://old.pinouts.ru/Game/CartridgeGameBoy_pinout.shtml) y descargado un cartucho en formato eagle
como base de [dwaq](https://github.com/dwaq/Homebrew-Gameboy-Cartridge) importo el documento en [kicad](http://kicad-pcb.org/), vacio todo menos el conector y la forma fisica del cartucho y comienzo a perfilar el disenyo, 2 dias despues este es el resultado:

PCB para conectar el puerto de cartucho de una game boy(5v+) a un sistema 3,3v+ como puede ser cualquier fpga o microcontrolador.

!["PCB BOTTOM"](screenshots/Gameboy_Cartridge_front.png "PCB BOTTOM")
!["PCB TOP"](screenshots/Gameboy_Cartridge_back.png "PCB TOP")

El render no a salido del todo bien, ese glich en la esquina superior derecha?? bueno no voy a perder mas el tiempo con eso,

Para realizar la conversion de nivel logico e utilizado el buffer noinversor CMOS [MC74VHC1GT125](https://www.onsemi.com/pub/Collateral/MC74VHC1GT125-D.PDF) su tiempo de [propagation delay](https://en.wikipedia.org/wiki/Propagation_delay) es de tPD = 3.5 ns (https://www.onsemi.com/pub/Collateral/MC74VHC1GT125-D.PDF) esto lo tendremos en cuenta ya que tenemos un tiempo limitado para colocar los datos en el bus.<br>
En la pagina [Dhole's blog](https://dhole.github.io/post/gameboy_cartridge_emu_1/) encontramos info sobre el timing de lectura/escritura.

Si quereis echarle un ojo a las placas aqui teneis el modelo 3d y los archivos de diseño.

3D WRLM Model [3D WRLM Model](https://gitlab.com/perromalo/gameboy-cart/tree/master/3D%20WRLM)<br>
Schematic [Schematic](https://gitlab.com/perromalo/gameboy-cart/blob/master/Gameboy_Cartridge.sch)<br>
Board [Board](https://gitlab.com/perromalo/gameboy-cart/blob/master/Gameboy_Cartridge.kicad_pcb)

Buscando info me encuentro con el repositorio de [Gekkio](https://github.com/Gekkio/gb-hardware) sitio con buena info sobre diseños de cartchos de gameboy, exelentre trabajo.

### B.O.M
```
Conector 2x19 2,54mm x 1
Capacitadores 0805 x 4
74AHC1G125 x 37
TLV75509PDBV x1 
```
### !!!! IMPORTANTE !!!!

AUN NO PROBADAS<br>
Cuando lleguen de china seguire adelante con el proyecto.

## Captura puerto link comunicaciones  

Empezamos a jugar con el conector link de la consola.<br>
Conector cable visto de enfrente
```
-----------------
|   2   4   6   |
\   1   3   5   /
 ---------------

5 negro      VDD35  =   +3,5 volts
6 rojo       SO     =   Data Out
3 amarillo   SI     =   DATA IN
4 marron     SD     =   Flow Control
2 azul       GND    =   Ground
1 blanco     SC     =   Clock
```
He cortado un cable y lo he soldado en una placa con varios jumpers para poder ir haciendo pruebas, lo primero es conectar un analizador logico y ver que nos encontramos.

Si no metemos ningun cartucho no hay ningun cambio en las lineas. Si ponemos el mario 2 tampoco sucede nada,con el tetris en single payer nada, pero cuando seleccionamos dos jugadores en la pantalla de inicio de tetris capturamos esta secuencia:

!["logic analyzer capture"](logic analizer.png "logic analyzer capture")
[Saleae Logic Software](https://www.saleae.com/downloads/) archivo captura [link](https://gitlab.com/perromalo/gameboy-cart/blob/master/24%20MHz,%201%20B%20Samples%20%5B17%5D.logicdata)

Ahora toca conectarla a alguna placa de prototipo rapido y empezar a hacer pruebas de envio de bytes.<br>
Para probar el envio y recepcion vamos a jugar con el juego tetris a 2 jugadores contra una placa arduino.<br>
Tendremos que estudiar el codigo ensamblador de la rom del tetris para comprender como podemos emular el funcionamiento del segundo jugador a traves del cable link.

## Diseccion del juego Tetris

Instalamos radare2, descargamos la rom de tetris de cualquier sitio de www, y abrimos la rom con el comando ´r2 nombre_rom.gb´ <br>
Ejecutamos el comando ´i´ q nos muestra informacion de la rom, lo mas interesante:
```
file     TETRIS
format   ningb
size     0x8000
humansz  32K
type     Gameboy-Rom
card	ROM
arch     gb
bits     16
endian   little
```
Ahora ejecutamos ´aaa´ que analizara la rom.<br>
RTFM encontramos que la interrrupcion del puerto serie esta en la direccion 0x58,<br>
ejecutamos ´s 0x58´ que nos movera a la direccion 0x58, r2 nos mostrara ´[0x00000058]>´ podemos consultar la ayuda de los comandos ej ´s?´<br>
ahora ejecutamos ´pd´ (print disasembly) y arriba de todo (hacer scroll arriba) encontramos:
```
[0x00000040]> s 0x58
[0x00000058]> pd
            ;-- Interrupt_Serial-Transfere:
        ,=< 0x00000058      c35b00         jp 0x005b
        |      ; JMP XREF from 0x00000058 (sym.Interrupt_Serial_Transfere + 0)             ???????????????????????
        `-> 0x0000005b      f5             push af
            0x0000005c      e5             push hl
            0x0000005d      d5             push de
            0x0000005e      c5             push bc
            0x0000005f  ~   cd6b00         call 0x006b                 ; fcn.0000006b
            ;-- Interrupt_Joypad:
            0x00000060      6b             ld l, e
```
vemos q desde la direccion 0x0000005b hasta 0x0000005e hace push de los registros af,hl,de,bc<br>
luego en 0x0000005f hacel un call a 0x006b para saltarse la interrupcion de joypad y continuar con el control de la interrupcion de control de flujo serie.<br>
los datos de rom del juego empiezan en la direccion 0x100 RTFM, asi que ejecutamos ´pd 0x79´ que nos muestra todo el codigo de la interrupcion serie (con la de joypad dentro).<br>
vemos que llega hasta 0x000000da, asi que ejecutamos ´pd 0x53´ y vemos toda la rutina.
```
[0x00000058]> pd 0x53
            ;-- Interrupt_Serial-Transfere:
        ,=< 0x00000058      c35b00         jp 0x005b
        |      ; JMP XREF from 0x00000058 (sym.Interrupt_Serial_Transfere + 0)
        `-> 0x0000005b      f5             push af
            0x0000005c      e5             push hl
            0x0000005d      d5             push de
            0x0000005e      c5             push bc
            0x0000005f  ~   cd6b00         call 0x006b                 ; fcn.0000006b
            ;-- Interrupt_Joypad:
            0x00000060      6b             ld l, e
            0x00000061      00             nop
            0x00000062      3e01           ld a, 0x01
            0x00000064      e0cc           ld [0xffcc], a
            0x00000066      c1             pop bc
            0x00000067      d1             pop de
            0x00000068      e1             pop hl
            0x00000069      f1             pop af
            0x0000006a      d9             reti
/ (fcn) fcn.0000006b 52
|   fcn.0000006b ();
|              ; UNKNOWN XREF from 0x0000005f (sym.Interrupt_Serial_Transfere + 7)
|              ; CALL XREF from 0x0000005f (sym.Interrupt_Serial_Transfere + 7)
|           0x0000006b      f0cd           ld a, [0xffcd]
|           0x0000006d      ef             rst 40                      ; sym.rst_40
|           0x0000006e      78             ld a, b
|           0x0000006f      00             nop
|           0x00000070      9f             sbc a
|           0x00000071      00             nop
|           0x00000072      a4             and h
|           0x00000073      00             nop
|           0x00000074      ba             cp d
|           0x00000075      00             nop
|           0x00000076      ea27f0         ld [0xf027], a
|           0x00000079      e1             pop hl
|           0x0000007a      fe07           cp 0x07
|       ,=< 0x0000007c      2808           jr Z, 0x08
|       |   0x0000007e      fe06           cp 0x06
|       |   0x00000080      c8             ret Z
|       |   0x00000081      3e06           ld a, 0x06
|       |   0x00000083      e0e1           ld [0xffe1], a
|       |   0x00000085      c9             ret
|       |      ; JMP XREF from 0x0000007c (fcn.0000006b)
|       `-> 0x00000086      f001           ld a, [0xff01]              ; Serial tranfer data
|           0x00000088      fe55           cp 0x55
|       ,=< 0x0000008a      2008           jr nZ, 0x08
|       |   0x0000008c      3e29           ld a, 0x29                  ; ')'
|       |   0x0000008e      e0cb           ld [0xffcb], a
|       |   0x00000090      3e01           ld a, 0x01
|      ,==< 0x00000092      1808           jr 0x08
|      ||      ; JMP XREF from 0x0000008a (fcn.0000006b)
|      |`-> 0x00000094      fe29           cp 0x29
|      |    0x00000096      c0             ret nZ
|      |    0x00000097      3e55           ld a, 0x55                  ; 'U'
|      |    0x00000099      e0cb           ld [0xffcb], a
|      |    0x0000009b      af             xor a
|      |       ; JMP XREF from 0x00000092 (fcn.0000006b)
|      `--> 0x0000009c      e002           ld [0xff02], a              ; Serial tranfer data - Ctl
\           0x0000009e      c9             ret
            0x0000009f      f001           ld a, [0xff01]              ; Serial tranfer data
            0x000000a1      e0d0           ld [0xffd0], a
            0x000000a3      c9             ret
            0x000000a4      f001           ld a, [0xff01]              ; Serial tranfer data
            0x000000a6      e0d0           ld [0xffd0], a
            0x000000a8      f0cb           ld a, [0xffcb]
            0x000000aa      fe29           cp 0x29
            0x000000ac      c8             ret Z
            0x000000ad      f0cf           ld a, [0xffcf]
            0x000000af      e001           ld [0xff01], a              ; Serial tranfer data
            0x000000b1      3eff           ld a, 0xff
            0x000000b3      e0cf           ld [0xffcf], a
            0x000000b5      3e80           ld a, 0x80
            0x000000b7      e002           ld [0xff02], a              ; Serial tranfer data - Ctl
            0x000000b9      c9             ret
            0x000000ba      f001           ld a, [0xff01]              ; Serial tranfer data
            0x000000bc      e0d0           ld [0xffd0], a
            0x000000be      f0cb           ld a, [0xffcb]
            0x000000c0      fe29           cp 0x29
            0x000000c2      c8             ret Z
            0x000000c3      f0cf           ld a, [0xffcf]
            0x000000c5      e001           ld [0xff01], a              ; Serial tranfer data
               ; CALL XREF from 0x00006648 (fcn.00006595 + 179)
            0x000000c7      fb             ei
            0x000000c8      cd980a         call fcn.00000a98
            0x000000cb      3e80           ld a, 0x80
            0x000000cd      e002           ld [0xff02], a              ; Serial tranfer data - Ctl
            0x000000cf      c9             ret
            0x000000d0      f0cd           ld a, [0xffcd]
            0x000000d2      fe02           cp 0x02
            0x000000d4      c0             ret nZ
            0x000000d5      af             xor a
            0x000000d6      e00f           ld [0xff0f], a              ; Interrupt Flag
            0x000000d8      fb             ei
            0x000000d9      c9             ret
            0x000000da      ff             rst 56
[0x00000058]> 
```
Este codigo tendremos que estudiarlo mas detenidamente, volveremos aqui despues de echar un ojo rapido por el codigo. Buscando rutinas que utilizen el puerto serie.

Para saber desde donde accede el juego a las llamadas de envio del puerto serie podemos hacer una busqueda en radare2 por el opcode (instruccion ASM) para la escritura por puerto serie que es ´e001´ podemos verlo en<br>
```
 0x000000c5      e001           ld [0xff01], a              ; Serial tranfer data
 ```
asi que ejecutamos ´s 0´ y ´pd 0x8000 ~ e001´ nos mostrara todas las lineas donde se ejecuta el opcode de envio
```
[0x00000100]> s 0
[0x00000000]> pd 0x8000 ~ e001
    ||:||   0x000000af      e001           ld [0xff01], a              ; Serial tranfer data
    ||:||   0x000000c5      e001           ld [0xff01], a              ; Serial tranfer data
  ||||:||   0x00000192      e001           ld [0xff01], a              ; Serial tranfer data
|   ||:::   0x0000022b      e001           ld [0xff01], a              ; Serial tranfer data
   |||:::   0x0000048d      e001           ld [0xff01], a              ; Serial tranfer data
  ||||:::   0x000004c7      e001           ld [0xff01], a              ; Serial tranfer data
|  |||:::   0x00000514      e001           ld [0xff01], a              ; Serial tranfer data
| ||||:::   0x00000522      e001           ld [0xff01], a              ; Serial tranfer data
   |||:::   0x000005df      e001           ld [0xff01], a              ; Serial tranfer data
  :|||:::   0x000006af      e001           ld [0xff01], a              ; Serial tranfer data
   |||:::   0x00000838      e001           ld [0xff01], a              ; Serial tranfer data
  --------> 0x000008fd      e001           ld [0xff01], a              ; Serial tranfer data
  ||||:::   0x0000091f      e001           ld [0xff01], a              ; Serial tranfer data
| ||||:::   0x0000097f      e001           ld [0xff01], a              ; Serial tranfer data
| ||||:::   0x0000099c      e001           ld [0xff01], a              ; Serial tranfer data
| ||||:::   0x000009b5      e001           ld [0xff01], a              ; Serial tranfer data
  :|||:::   0x00000a0e      e001           ld [0xff01], a              ; Serial tranfer data
  :|||:::   0x00000a29      e001           ld [0xff01], a              ; Serial tranfer data
  :|||:::   0x00000a46      e001           ld [0xff01], a              ; Serial tranfer data
  :|||:::   0x00000a5f      e001           ld [0xff01], a              ; Serial tranfer data
  :|||:::   0x00000a78      e001           ld [0xff01], a              ; Serial tranfer data
   |||:::   0x00001449      e001           ld [0xff01], a              ; Serial tranfer data
  :||::::   0x00003b10      e001           ld [0xff01], a              ; Serial tranfer data
[0x00000000]> 
```
21 lineas iremos echando un vistazo... <br>
antes ya hemos visto las dos primeras, la de la direccion 0xaf y 0xc5 que eran rutinas de la interrupcion serie.<br>

Tambien podriamos buscar los opcodes de cuando leemos de la direccion de datos serie y lo mete en el registro ´a´ para ello ejecutamos ´pd 0x8000 ~ f001´
```
[0x00000000]> pd 0x8000 ~ f001
|  `------> 0x00000086      f001           ld a, [0xff01]              ; Serial tranfer data
    ||:||   0x0000009f      f001           ld a, [0xff01]              ; Serial tranfer data
    ||:||   0x000000a4      f001           ld a, [0xff01]              ; Serial tranfer data
    ||:||   0x000000ba      f001           ld a, [0xff01]              ; Serial tranfer data
  ||||:::   0x00000908      f001           ld a, [0xff01]              ; Serial tranfer data
| ||||:::   0x0000098a      f001           ld a, [0xff01]              ; Serial tranfer data
| ||||:::   0x000009c0      f001           ld a, [0xff01]              ; Serial tranfer data
  :|||:::   0x00000a19      f001           ld a, [0xff01]              ; Serial tranfer data
  :|||:::   0x00000a34      f001           ld a, [0xff01]              ; Serial tranfer data
  :|||:::   0x00000a51      f001           ld a, [0xff01]              ; Serial tranfer data
  :|||:::   0x00000a6a      f001           ld a, [0xff01]              ; Serial tranfer data
  :|||:::   0x00000a83      f001           ld a, [0xff01]              ; Serial tranfer data
  : :| ::   0x00006dd9      f001           ld a, [0xff01]              ; Serial tranfer data
[0x00000000]> 
```
13 direcciones de memoria del cartucho tetris contienen lecturas de la direccion de datos del puerto serie al registro ´a´.

Si nos movemos a la direccion s 0x17e y ejecutamos pd 0x11 nos muestra una rutina llamada desde la interrupcion de vertical video (cada vez q el display refresca imagen) q utiliza el puerto serie.
```
[0x0000017e]> pd 0x11
               ; JMP XREF from 0x00000040 (sym.Interrupt_Vblank + 0)
            0x0000017e      f5             push af
            0x0000017f      c5             push bc
            0x00000180      d5             push de
            0x00000181      e5             push hl
            0x00000182      f0ce           ld a, [0xffce]
            0x00000184      a7             and a
        ,=< 0x00000185      2812           jr Z, 0x12
        |   0x00000187      f0cb           ld a, [0xffcb]
        |   0x00000189      fe29           cp 0x29
       ,==< 0x0000018b      200c           jr nZ, 0x0c
       ||   0x0000018d      af             xor a
       ||   0x0000018e      e0ce           ld [0xffce], a
       ||   0x00000190      f0cf           ld a, [0xffcf]
       ||   0x00000192      e001           ld [0xff01], a              ; Serial tranfer data
       ||   0x00000194      2102ff         ld hl, 0xff02
       ||   0x00000197      3681           ld [hl], 0x81
       ||      ; JMP XREF from 0x00000185 (fcn.00000166 + 31)
       ||      ; JMP XREF from 0x0000018b (fcn.00000166 + 37)
       ``-> 0x00000199      cde021         call fcn.000021e0
[0x0000017e]> 
```
siguiendo buscando encontramos en la direccion 0x219 el siguiente codigo:
```
[0x00000219]> pd 0x10
|       `=< 0x00000219      20f9           jr nZ, 0xf9
|              ; JMP XREF from 0x000002d3 (sym.rst_8)
|              ; JMP XREF from 0x00001c13 (fcn.00001c0d)
|           0x0000021b      3e01           ld a, 0x01
|           0x0000021d      f3             di
|           0x0000021e      e00f           ld [0xff0f], a              ; Interrupt Flag
|           0x00000220      e0ff           ld [0xffff], a              ; Interrupt Enable Flag
|           0x00000222      af             xor a
|           0x00000223      e042           ld [0xff42], a              ; LCDC - Scroll y
|           0x00000225      e043           ld [0xff43], a              ; LCDC - Scroll x
|           0x00000227      e0a4           ld [0xffa4], a
|           0x00000229      e041           ld [0xff41], a              ; LCDC - STAT
|           0x0000022b      e001           ld [0xff01], a              ; Serial tranfer data
|           0x0000022d      e002           ld [0xff02], a              ; Serial tranfer data - Ctl
|           0x0000022f      3e80           ld a, 0x80
|           0x00000231      e040           ld [0xff40], a              ; LCDC
|              ; JMP XREF from 0x00000237 (sym.rst_8)
|           0x00000233      f044           ld a, [0xff44]              ; LCDC - y cord
|           0x00000235      fe94           cp 0x94
```
aqui encontramos que activa las interrupciones por vblank y puerto serie
```
  0x000002e9      3e09           ld a, 0x09
  0x000002eb      e0ff           ld [0xffff], a              ; Interrupt Enable Flag
```
Luego encontramos en la direccion 0x488:
 ```
 ; JMP XREF from 0x0000047c (fcn.000002f8 + 388)
            0x00000488      cd980a         call fcn.00000a98
            0x0000048b      3e55           ld a, 0x55                  ; 'U'
            0x0000048d      e001           ld [0xff01], a              ; Serial tranfer data
            0x0000048f      3e80           ld a, 0x80
            0x00000491      e002           ld [0xff02], a              ; Serial tranfer data - Ctl 
            0x00000493      f0cc           ld a, [0xffcc]
            0x00000495      a7             and a
            0x00000496      280a           jr Z, 0x0a
            0x00000498      f0cb           ld a, [0xffcb]
            0x0000049a      a7             and a
            0x0000049b      203a           jr nZ, 0x3a
            0x0000049d      af             xor a
            0x0000049e      e0cc           ld [0xffcc], a
            0x000004a0      1867           jr 0x67
```
Con lo que hemos ido viendo hasta ahora el resumen de las llamadas a la instruccion de envio quedaria asi
```
    ||:||   0x000000af  Interrupcion serie
    ||:||   0x000000c5  Interrupcion serie
  ||||:||   0x00000192  Interrupcion VBlank
|   ||:::   0x0000022b  ?
   |||:::   0x0000048d  ?
  ||||:::   0x000004c7
|  |||:::   0x00000514
| ||||:::   0x00000522
   |||:::   0x000005df
  :|||:::   0x000006af
   |||:::   0x00000838
```


Para ir entreteniendos un poco mientras buscamos mas rutinas vamos a ver que pasa en las llamadas al puerto serie.<br>
Vamos a ir explicando el siguiente codigo 
```
            0x0000048b      3e55           ld a, 0x55                  ; 'U'
            0x0000048d      e001           ld [0xff01], a              ; Serial tranfer data
            0x0000048f      3e80           ld a, 0x80
            0x00000491      e002           ld [0xff02], a              ; Serial tranfer data - Ctl    
```
carga en el registro ´a´ el valor 0x55<br>
en la direccion 0xFF01 carga el contenido del registro ´a´ (lo que va a enviar)<br>
en la direccion 0xFF02 carga  0x80  q es para las opciones de control del envio<br>
```
    Bit 7 activo para comenzar a transmitir, 0 no transmitir.
    Bit 1 Velocidad de reloj, 0 normal, 1 para rapido (solo gameboy color)
    Bit 0 Reloj externo = 0, Reloj interno = 1.
```
carga  0x80 que quiere decir que solo activa el bit 7, osea recibir a velocidad normal y reloj externo.<br>
La velocidad normal es de 8192Hz -  1KB/s - Bit 1 cleared, Normal<br>
Este trozo de codigo era para pedir recepcion de datos, ahora veremos para enviar nosotros.<br>
