EESchema Schematic File Version 4
LIBS:Gameboy_Cartridge-cache
EELAYER 29 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Wire Wire Line
	1050 2400 750  2400
Text Label 750  2400 0    70   ~ 0
VCC
Wire Wire Line
	1350 2400 1450 2400
Text Label 1450 2400 0    70   ~ 0
GND
$Comp
L Gameboy_Cartridge-eagle-import:CONNECTOR U$1
U 1 0 850A8AB0BABD3B23
P 2700 5700
F 0 "U$1" H 2700 5400 42  0000 L BNN
F 1 "CONNECTOR" V 2550 5550 42  0000 L BNN
F 2 "Gameboy_Cartridge:CONNECTOR" H 2700 5700 50  0001 C CNN
F 3 "" H 2700 5700 50  0001 C CNN
	1    2700 5700
	1    0    0    -1  
$EndComp
$Comp
L Gameboy_Cartridge-eagle-import:C-EUC0805 C1
U 1 0 12C3E9BA86312766
P 1150 2400
F 0 "C1" H 1210 2415 59  0000 L BNN
F 1 "20nF" H 1210 2215 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1150 2400 50  0001 C CNN
F 3 "" H 1150 2400 50  0001 C CNN
	1    1150 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7000 5600 7000 5350
Wire Wire Line
	7200 5600 7200 5350
Wire Wire Line
	7400 5600 7400 5350
Wire Wire Line
	7600 5600 7600 5350
Wire Wire Line
	7800 5600 7800 5350
Wire Wire Line
	8000 5600 8000 5350
Wire Wire Line
	8200 5600 8200 5350
Wire Wire Line
	8400 5600 8400 5350
Text Label 8400 5350 1    50   ~ 0
D7
Text Label 8200 5350 1    50   ~ 0
D6
Text Label 8000 5350 1    50   ~ 0
D5
Text Label 7800 5350 1    50   ~ 0
D4
Text Label 7600 5350 1    50   ~ 0
D3
Text Label 7400 5350 1    50   ~ 0
D2
Text Label 7200 5350 1    50   ~ 0
D1
Text Label 7000 5350 1    50   ~ 0
D0
Wire Wire Line
	8600 5600 8600 5350
Wire Wire Line
	8800 5600 8800 5350
Wire Wire Line
	9000 5600 9000 5350
Wire Wire Line
	6800 5600 6800 5350
Wire Wire Line
	6600 5600 6600 5350
Wire Wire Line
	6400 5600 6400 5350
Wire Wire Line
	6200 5600 6200 5350
Wire Wire Line
	6000 5600 6000 5350
Wire Wire Line
	5800 5600 5800 5350
Wire Wire Line
	5600 5600 5600 5350
Wire Wire Line
	5400 5600 5400 5350
Wire Wire Line
	5200 5600 5200 5350
Wire Wire Line
	5000 5600 5000 5350
Wire Wire Line
	4800 5600 4800 5350
Wire Wire Line
	4600 5600 4600 5350
Wire Wire Line
	4400 5600 4400 5350
Wire Wire Line
	4200 5600 4200 5350
Wire Wire Line
	4000 5600 4000 5350
Wire Wire Line
	3800 5600 3800 5350
Wire Wire Line
	3600 5600 3600 5350
Wire Wire Line
	3400 5600 3400 5350
Wire Wire Line
	3200 5600 3200 5350
Wire Wire Line
	2800 5600 2800 5350
Text Label 2800 5350 1    50   ~ 0
VCC
Wire Wire Line
	3000 5600 3000 5350
Text Label 3000 5350 1    50   ~ 0
CLK
Text Label 3200 5350 1    50   ~ 0
!WR
Text Label 3400 5350 1    50   ~ 0
!RD
Text Label 3600 5350 1    50   ~ 0
!CS
Text Label 3800 5350 1    50   ~ 0
A0
Text Label 4000 5350 1    50   ~ 0
A1
Text Label 4200 5350 1    50   ~ 0
A2
Text Label 4400 5350 1    50   ~ 0
A3
Text Label 4600 5350 1    50   ~ 0
A4
Text Label 4800 5350 1    50   ~ 0
A5
Text Label 5000 5350 1    50   ~ 0
A6
Text Label 5200 5350 1    50   ~ 0
A7
Text Label 5400 5350 1    50   ~ 0
A8
Text Label 5600 5350 1    50   ~ 0
A9
Text Label 5800 5350 1    50   ~ 0
A10
Text Label 6000 5350 1    50   ~ 0
A11
Text Label 6200 5350 1    50   ~ 0
A12
Text Label 6400 5350 1    50   ~ 0
A13
Text Label 6600 5350 1    50   ~ 0
A14
Text Label 6800 5350 1    50   ~ 0
A15
Text Label 8600 5350 1    50   ~ 0
!RST
Text Label 8800 5350 1    50   ~ 0
AUDIO_IN
Text Label 9000 5350 1    50   ~ 0
GND
Wire Wire Line
	14050 250  13800 250 
Wire Wire Line
	14600 250  14950 250 
Wire Wire Line
	14350 50   14350 -100
Wire Wire Line
	14350 -100 14550 -100
Text Label 14550 -100 0    50   ~ 0
OE1
Text Label 14950 250  0    50   ~ 0
D0
Text Label 13800 250  2    50   ~ 0
OD0
Wire Wire Line
	14050 1150 13800 1150
Wire Wire Line
	14600 1150 14950 1150
Wire Wire Line
	14350 950  14350 800 
Wire Wire Line
	14350 800  14550 800 
Text Label 14550 800  0    50   ~ 0
OE1
Text Label 16800 250  0    50   ~ 0
D1
Text Label 15650 250  2    50   ~ 0
OD1
Wire Wire Line
	15900 250  15650 250 
Wire Wire Line
	16450 250  16800 250 
Wire Wire Line
	16200 50   16200 -100
Wire Wire Line
	16200 -100 16400 -100
Text Label 16400 -100 0    50   ~ 0
OE1
Text Label 14950 2050 0    50   ~ 0
D4
Text Label 13800 2050 2    50   ~ 0
OD4
Wire Wire Line
	15900 1150 15650 1150
Wire Wire Line
	16450 1150 16800 1150
Wire Wire Line
	16200 950  16200 800 
Wire Wire Line
	16200 800  16400 800 
Text Label 16400 800  0    50   ~ 0
OE1
Text Label 16800 2050 0    50   ~ 0
D5
Text Label 15650 2050 2    50   ~ 0
OD5
Wire Wire Line
	14050 2050 13800 2050
Wire Wire Line
	14600 2050 14950 2050
Wire Wire Line
	14350 1850 14350 1700
Wire Wire Line
	14350 1700 14550 1700
Text Label 14550 1700 0    50   ~ 0
OE1
Text Label 16800 1150 0    50   ~ 0
D2
Text Label 15650 1150 2    50   ~ 0
OD2
Wire Wire Line
	14050 2950 13800 2950
Wire Wire Line
	14600 2950 14950 2950
Wire Wire Line
	14350 2750 14350 2600
Wire Wire Line
	14350 2600 14550 2600
Text Label 14550 2600 0    50   ~ 0
OE1
Text Label 14950 1150 0    50   ~ 0
D3
Text Label 13800 1150 2    50   ~ 0
OD3
Wire Wire Line
	15900 2050 15650 2050
Wire Wire Line
	16450 2050 16800 2050
Wire Wire Line
	16200 1850 16200 1700
Wire Wire Line
	16200 1700 16400 1700
Text Label 16400 1700 0    50   ~ 0
OE1
Text Label 16800 2950 0    50   ~ 0
D6
Text Label 15650 2950 2    50   ~ 0
OD6
Wire Wire Line
	15900 2950 15650 2950
Wire Wire Line
	16450 2950 16800 2950
Wire Wire Line
	16200 2750 16200 2600
Wire Wire Line
	16200 2600 16400 2600
Text Label 16400 2600 0    50   ~ 0
OE1
Text Label 14950 2950 0    50   ~ 0
D7
Text Label 13800 2950 2    50   ~ 0
OD7
Wire Wire Line
	18500 300  18250 300 
Wire Wire Line
	19050 300  19400 300 
Wire Wire Line
	18800 100  18800 -50 
Wire Wire Line
	18800 -50  19000 -50 
Text Label 19000 -50  0    50   ~ 0
OE2
Text Label 18250 300  2    50   ~ 0
D0
Text Label 19400 300  0    50   ~ 0
OD0
Wire Wire Line
	18500 1200 18250 1200
Wire Wire Line
	19050 1200 19400 1200
Wire Wire Line
	18800 1000 18800 850 
Wire Wire Line
	18800 850  19000 850 
Text Label 19000 850  0    50   ~ 0
OE2
Text Label 20100 300  2    50   ~ 0
D1
Text Label 21250 300  0    50   ~ 0
OD1
Wire Wire Line
	20350 300  20100 300 
Wire Wire Line
	20900 300  21250 300 
Wire Wire Line
	20650 100  20650 -50 
Wire Wire Line
	20650 -50  20850 -50 
Text Label 20850 -50  0    50   ~ 0
OE2
Text Label 18250 2100 2    50   ~ 0
D4
Text Label 19400 2100 0    50   ~ 0
OD4
Wire Wire Line
	20350 1200 20100 1200
Wire Wire Line
	20900 1200 21250 1200
Wire Wire Line
	20650 1000 20650 850 
Wire Wire Line
	20650 850  20850 850 
Text Label 20850 850  0    50   ~ 0
OE2
Text Label 20100 2100 2    50   ~ 0
D5
Text Label 21250 2100 0    50   ~ 0
OD5
Wire Wire Line
	18500 2100 18250 2100
Wire Wire Line
	19050 2100 19400 2100
Wire Wire Line
	18800 1900 18800 1750
Wire Wire Line
	18800 1750 19000 1750
Text Label 19000 1750 0    50   ~ 0
OE2
Text Label 20100 1200 2    50   ~ 0
D2
Text Label 21250 1200 0    50   ~ 0
OD2
Wire Wire Line
	18500 3000 18250 3000
Wire Wire Line
	19050 3000 19400 3000
Wire Wire Line
	18800 2800 18800 2650
Wire Wire Line
	18800 2650 19000 2650
Text Label 19000 2650 0    50   ~ 0
OE2
Text Label 18250 1200 2    50   ~ 0
D3
Text Label 19400 1200 0    50   ~ 0
OD3
Wire Wire Line
	20350 2100 20100 2100
Wire Wire Line
	20900 2100 21250 2100
Wire Wire Line
	20650 1900 20650 1750
Wire Wire Line
	20650 1750 20850 1750
Text Label 20850 1750 0    50   ~ 0
OE2
Text Label 20100 3000 2    50   ~ 0
D6
Text Label 21250 3000 0    50   ~ 0
OD6
Wire Wire Line
	20350 3000 20100 3000
Wire Wire Line
	20900 3000 21250 3000
Wire Wire Line
	20650 2800 20650 2650
Wire Wire Line
	20650 2650 20850 2650
Text Label 20850 2650 0    50   ~ 0
OE2
Text Label 18250 3000 2    50   ~ 0
D7
Text Label 19400 3000 0    50   ~ 0
OD7
$Comp
L 74xGxx:74AHC1G125 U1
U 1 1 5CA67313
P 14350 250
F 0 "U1" H 14325 76  50  0000 C CNN
F 1 "74AHC1G125" H 14325 -15 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 14350 250 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 14350 250 50  0001 C CNN
	1    14350 250 
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U5
U 1 1 5CA67845
P 16200 250
F 0 "U5" H 16175 76  50  0000 C CNN
F 1 "74AHC1G125" H 16175 -15 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 16200 250 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 16200 250 50  0001 C CNN
	1    16200 250 
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U6
U 1 1 5CA67CA4
P 16200 1150
F 0 "U6" H 16175 976 50  0000 C CNN
F 1 "74AHC1G125" H 16175 885 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 16200 1150 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 16200 1150 50  0001 C CNN
	1    16200 1150
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U2
U 1 1 5CA681F9
P 14350 1150
F 0 "U2" H 14325 976 50  0000 C CNN
F 1 "74AHC1G125" H 14325 885 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 14350 1150 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 14350 1150 50  0001 C CNN
	1    14350 1150
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U3
U 1 1 5CA6CA2B
P 14350 2050
F 0 "U3" H 14325 1876 50  0000 C CNN
F 1 "74AHC1G125" H 14325 1785 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 14350 2050 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 14350 2050 50  0001 C CNN
	1    14350 2050
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U4
U 1 1 5CA6CE93
P 14350 2950
F 0 "U4" H 14325 2776 50  0000 C CNN
F 1 "74AHC1G125" H 14325 2685 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 14350 2950 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 14350 2950 50  0001 C CNN
	1    14350 2950
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U7
U 1 1 5CA6D7D4
P 16200 2050
F 0 "U7" H 16175 1876 50  0000 C CNN
F 1 "74AHC1G125" H 16175 1785 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 16200 2050 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 16200 2050 50  0001 C CNN
	1    16200 2050
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U8
U 1 1 5CA6DE1E
P 16200 2950
F 0 "U8" H 16175 2776 50  0000 C CNN
F 1 "74AHC1G125" H 16175 2685 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 16200 2950 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 16200 2950 50  0001 C CNN
	1    16200 2950
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U12
U 1 1 5CA71550
P 18800 3000
F 0 "U12" H 18775 2826 50  0000 C CNN
F 1 "74AHC1G125" H 18775 2735 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 18800 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 18800 3000 50  0001 C CNN
	1    18800 3000
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U11
U 1 1 5CA71A29
P 18800 2100
F 0 "U11" H 18775 1926 50  0000 C CNN
F 1 "74AHC1G125" H 18775 1835 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 18800 2100 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 18800 2100 50  0001 C CNN
	1    18800 2100
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U15
U 1 1 5CA71DF0
P 20650 2100
F 0 "U15" H 20625 1926 50  0000 C CNN
F 1 "74AHC1G125" H 20625 1835 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 20650 2100 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 20650 2100 50  0001 C CNN
	1    20650 2100
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U16
U 1 1 5CA7220D
P 20650 3000
F 0 "U16" H 20625 2826 50  0000 C CNN
F 1 "74AHC1G125" H 20625 2735 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 20650 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 20650 3000 50  0001 C CNN
	1    20650 3000
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U9
U 1 1 5CA76AE0
P 18800 300
F 0 "U9" H 18775 126 50  0000 C CNN
F 1 "74AHC1G125" H 18775 35  50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 18800 300 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 18800 300 50  0001 C CNN
	1    18800 300 
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U10
U 1 1 5CA76FBA
P 18800 1200
F 0 "U10" H 18775 1026 50  0000 C CNN
F 1 "74AHC1G125" H 18775 935 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 18800 1200 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 18800 1200 50  0001 C CNN
	1    18800 1200
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U14
U 1 1 5CA774DB
P 20650 1200
F 0 "U14" H 20625 1026 50  0000 C CNN
F 1 "74AHC1G125" H 20625 935 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 20650 1200 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 20650 1200 50  0001 C CNN
	1    20650 1200
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U13
U 1 1 5CA77C5F
P 20650 300
F 0 "U13" H 20625 126 50  0000 C CNN
F 1 "74AHC1G125" H 20625 35  50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 20650 300 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 20650 300 50  0001 C CNN
	1    20650 300 
	1    0    0    -1  
$EndComp
Wire Wire Line
	18550 50   18550 -50 
Wire Wire Line
	18550 -50  18350 -50 
Text Label 18350 -50  2    50   ~ 0
VCC2
Wire Wire Line
	18550 950  18550 850 
Wire Wire Line
	18550 850  18350 850 
Text Label 18350 850  2    50   ~ 0
VCC2
Wire Wire Line
	20400 50   20400 -50 
Wire Wire Line
	20400 -50  20200 -50 
Text Label 20200 -50  2    50   ~ 0
VCC2
Wire Wire Line
	20400 950  20400 850 
Wire Wire Line
	20400 850  20200 850 
Text Label 20200 850  2    50   ~ 0
VCC2
Wire Wire Line
	15950 0    15950 -100
Wire Wire Line
	15950 -100 15750 -100
Text Label 15750 -100 2    50   ~ 0
VCC
Wire Wire Line
	14100 0    14100 -100
Wire Wire Line
	14100 -100 13900 -100
Text Label 13900 -100 2    50   ~ 0
VCC
Wire Wire Line
	15950 900  15950 800 
Wire Wire Line
	15950 800  15750 800 
Text Label 15750 800  2    50   ~ 0
VCC
Wire Wire Line
	14100 900  14100 800 
Wire Wire Line
	14100 800  13900 800 
Text Label 13900 800  2    50   ~ 0
VCC
Wire Wire Line
	15950 1800 15950 1700
Wire Wire Line
	15950 1700 15750 1700
Text Label 15750 1700 2    50   ~ 0
VCC
Wire Wire Line
	14100 1800 14100 1700
Wire Wire Line
	14100 1700 13900 1700
Text Label 13900 1700 2    50   ~ 0
VCC
Wire Wire Line
	15950 2700 15950 2600
Wire Wire Line
	15950 2600 15750 2600
Text Label 15750 2600 2    50   ~ 0
VCC
Wire Wire Line
	14100 2700 14100 2600
Wire Wire Line
	14100 2600 13900 2600
Text Label 13900 2600 2    50   ~ 0
VCC
Wire Wire Line
	20400 1850 20400 1750
Wire Wire Line
	20400 1750 20200 1750
Text Label 20200 1750 2    50   ~ 0
VCC2
Wire Wire Line
	18550 1850 18550 1750
Wire Wire Line
	18550 1750 18350 1750
Text Label 18350 1750 2    50   ~ 0
VCC2
Wire Wire Line
	20400 2750 20400 2650
Wire Wire Line
	20400 2650 20200 2650
Text Label 20200 2650 2    50   ~ 0
VCC2
Wire Wire Line
	18550 2750 18550 2650
Wire Wire Line
	18550 2650 18350 2650
Text Label 18350 2650 2    50   ~ 0
VCC2
Wire Notes Line
	17300 -500 13200 -500
Wire Notes Line
	13200 -500 13200 3550
Wire Notes Line
	13200 3550 17300 3550
Wire Notes Line
	17300 -500 17300 3550
Wire Notes Line
	17850 -500 21800 -500
Wire Notes Line
	21800 -500 21800 3550
Wire Notes Line
	21800 3550 17850 3550
Wire Notes Line
	17850 3550 17850 -500
Text Notes 14050 -750 0    168  ~ 0
DATA IN   3.3v to 5v
Text Notes 18250 -750 0    168  ~ 0
DATA OUT   5v to 3.3v
Wire Wire Line
	13950 6450 13700 6450
Wire Wire Line
	14500 6450 14850 6450
Wire Wire Line
	14250 6250 14250 6100
Wire Wire Line
	14250 6100 14450 6100
Text Label 14450 6100 0    50   ~ 0
OE3
Text Label 13700 6450 2    50   ~ 0
CLK
Text Label 14850 6450 0    50   ~ 0
OCLK
Wire Wire Line
	15800 6450 15550 6450
Wire Wire Line
	16350 6450 16700 6450
Wire Wire Line
	16100 6250 16100 6100
Wire Wire Line
	16100 6100 16300 6100
Text Label 16300 6100 0    50   ~ 0
OE3
Text Label 15550 7350 2    50   ~ 0
!RD
Text Label 16700 7350 0    50   ~ 0
O!RD
Wire Wire Line
	13950 7350 13700 7350
Wire Wire Line
	14500 7350 14850 7350
Wire Wire Line
	14250 7150 14250 7000
Wire Wire Line
	14250 7000 14450 7000
Text Label 14450 7000 0    50   ~ 0
OE3
Text Label 13700 7350 2    50   ~ 0
!WR
Text Label 14850 7350 0    50   ~ 0
O!WR
Wire Wire Line
	13950 8250 13700 8250
Wire Wire Line
	14500 8250 14850 8250
Wire Wire Line
	14250 8050 14250 7900
Wire Wire Line
	14250 7900 14450 7900
Text Label 14450 7900 0    50   ~ 0
OE3
Text Label 13700 8250 2    50   ~ 0
!RST
Text Label 14850 8250 0    50   ~ 0
O!RST
Wire Wire Line
	15800 7350 15550 7350
Wire Wire Line
	16350 7350 16700 7350
Wire Wire Line
	16100 7150 16100 7000
Wire Wire Line
	16100 7000 16300 7000
Text Label 16300 7000 0    50   ~ 0
OE3
Text Label 15550 6450 2    50   ~ 0
!CS
Text Label 16700 6450 0    50   ~ 0
O!CS
$Comp
L 74xGxx:74AHC1G125 U19
U 1 1 5CB8E20E
P 14250 8250
F 0 "U19" H 14225 8076 50  0000 C CNN
F 1 "74AHC1G125" H 14225 7985 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 14250 8250 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 14250 8250 50  0001 C CNN
	1    14250 8250
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U18
U 1 1 5CB8E218
P 14250 7350
F 0 "U18" H 14225 7176 50  0000 C CNN
F 1 "74AHC1G125" H 14225 7085 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 14250 7350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 14250 7350 50  0001 C CNN
	1    14250 7350
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U21
U 1 1 5CB8E222
P 16100 7350
F 0 "U21" H 16075 7176 50  0000 C CNN
F 1 "74AHC1G125" H 16075 7085 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 16100 7350 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 16100 7350 50  0001 C CNN
	1    16100 7350
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U17
U 1 1 5CB8E236
P 14250 6450
F 0 "U17" H 14225 6276 50  0000 C CNN
F 1 "74AHC1G125" H 14225 6185 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 14250 6450 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 14250 6450 50  0001 C CNN
	1    14250 6450
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U20
U 1 1 5CB8E240
P 16100 6450
F 0 "U20" H 16075 6276 50  0000 C CNN
F 1 "74AHC1G125" H 16075 6185 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 16100 6450 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H 16100 6450 50  0001 C CNN
	1    16100 6450
	1    0    0    -1  
$EndComp
Wire Wire Line
	14000 6200 14000 6100
Wire Wire Line
	14000 6100 13800 6100
Text Label 13800 6100 2    50   ~ 0
VCC2
Wire Wire Line
	15850 6200 15850 6100
Wire Wire Line
	15850 6100 15650 6100
Text Label 15650 6100 2    50   ~ 0
VCC2
Wire Wire Line
	15850 7100 15850 7000
Wire Wire Line
	15850 7000 15650 7000
Text Label 15650 7000 2    50   ~ 0
VCC2
Wire Wire Line
	14000 7100 14000 7000
Wire Wire Line
	14000 7000 13800 7000
Text Label 13800 7000 2    50   ~ 0
VCC2
Wire Wire Line
	14000 8000 14000 7900
Wire Wire Line
	14000 7900 13800 7900
Text Label 13800 7900 2    50   ~ 0
VCC2
Wire Notes Line
	13100 8950 17350 8950
Wire Notes Line
	17350 8950 17350 5600
Wire Notes Line
	17350 5600 13100 5600
Wire Notes Line
	13100 5600 13100 8950
Text Notes 13500 5250 0    168  ~ 0
CONTROL OUT   5v to 3.3v
$Comp
L Connector_Generic:Conn_02x19_Odd_Even J1
U 1 1 5CBF4485
P 5250 2350
F 0 "J1" H 5300 3467 50  0000 C CNN
F 1 "Conn_02x19_Odd_Even" H 5300 3376 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x19_P2.54mm_Vertical" H 5250 2350 50  0001 C CNN
F 3 "~" H 5250 2350 50  0001 C CNN
	1    5250 2350
	1    0    0    -1  
$EndComp
Wire Wire Line
	5050 1450 4650 1450
Wire Wire Line
	5050 1550 4650 1550
Wire Wire Line
	5050 1650 4650 1650
Wire Wire Line
	5050 1750 4650 1750
Wire Wire Line
	5050 1850 4650 1850
Wire Wire Line
	5050 1950 4650 1950
Wire Wire Line
	5050 2050 4650 2050
Wire Wire Line
	5050 2150 4650 2150
Wire Wire Line
	5050 2250 4650 2250
Wire Wire Line
	5050 2350 4650 2350
Wire Wire Line
	5050 2450 4650 2450
Wire Wire Line
	5050 2550 4650 2550
Wire Wire Line
	5050 2650 4650 2650
Wire Wire Line
	5050 2750 4650 2750
Wire Wire Line
	5050 2850 4650 2850
Wire Wire Line
	5050 2950 4650 2950
Wire Wire Line
	5050 3050 4650 3050
Wire Wire Line
	5050 3150 4650 3150
Wire Wire Line
	5050 3250 4650 3250
Wire Wire Line
	5950 3250 5550 3250
Wire Wire Line
	5550 3150 5950 3150
Wire Wire Line
	5550 3050 5950 3050
Wire Wire Line
	5550 2950 5950 2950
Wire Wire Line
	5550 2850 5950 2850
Wire Wire Line
	5550 2750 5950 2750
Wire Wire Line
	5550 2650 5950 2650
Wire Wire Line
	5550 2550 5950 2550
Wire Wire Line
	5550 2450 5950 2450
Wire Wire Line
	5550 2350 5950 2350
Wire Wire Line
	5550 2250 5950 2250
Wire Wire Line
	5550 2150 5950 2150
Wire Wire Line
	5550 2050 5950 2050
Wire Wire Line
	5550 1950 5950 1950
Wire Wire Line
	5550 1850 5950 1850
Wire Wire Line
	5550 1750 5950 1750
Wire Wire Line
	5550 1650 5950 1650
Wire Wire Line
	5550 1550 5950 1550
Wire Wire Line
	5550 1450 5950 1450
Text Label 5950 1450 0    50   ~ 0
OE3
Text Label 5950 1650 0    50   ~ 0
O!CS
Text Label 5950 1550 0    50   ~ 0
O!RD
Text Label 4650 1850 2    50   ~ 0
OA5
Text Label 4650 1750 2    50   ~ 0
OA0
Text Label 5950 2150 0    50   ~ 0
OA10
Text Label 5950 2050 0    50   ~ 0
OA8
Text Label 4650 2250 2    50   ~ 0
OA6
Text Label 4650 2350 2    50   ~ 0
GND
Text Label 4650 2450 2    50   ~ 0
OA14
Text Label 4650 2550 2    50   ~ 0
GND
Text Label 4650 2650 2    50   ~ 0
OA15
Text Label 4650 3150 2    50   ~ 0
OD4
Text Label 5950 3050 0    50   ~ 0
OD5
Text Label 4650 2950 2    50   ~ 0
GND
Text Label 5950 3150 0    50   ~ 0
OD6
Text Label 4650 3250 2    50   ~ 0
OD7
Text Label 5950 3250 0    50   ~ 0
OE2
Text Label 4650 3050 2    50   ~ 0
OD3
Text Label 4650 2850 2    50   ~ 0
OD2
Text Label 4650 2750 2    50   ~ 0
OD1
Text Label 5950 2950 0    50   ~ 0
GND
Text Label 5950 2850 0    50   ~ 0
OD0
Text Label 5950 2750 0    50   ~ 0
OE1
Text Label 5950 2650 0    50   ~ 0
OA12
Text Label 5950 2550 0    50   ~ 0
GND
Text Label 5950 2450 0    50   ~ 0
OA13
Text Label 5950 2350 0    50   ~ 0
GND
Text Label 5950 2250 0    50   ~ 0
OA7
Text Label 4650 2150 2    50   ~ 0
OA9
Text Label 4650 2050 2    50   ~ 0
OA11
Text Label 4650 1950 2    50   ~ 0
OA1
Text Label 5950 1750 0    50   ~ 0
OA3
Text Label 4650 1650 2    50   ~ 0
O!WR
Text Label 4650 1550 2    50   ~ 0
O!RST
Text Label 4650 1450 2    50   ~ 0
OCLK
Wire Wire Line
	-4050 800  -4300 800 
Wire Wire Line
	-3500 800  -3150 800 
Wire Wire Line
	-3750 600  -3750 450 
Wire Wire Line
	-3750 450  -3550 450 
Text Label -3550 450  0    50   ~ 0
OE3
Text Label -4250 4500 2    50   ~ 0
A0
Text Label -3100 4500 0    50   ~ 0
OA0
Wire Wire Line
	-4050 1700 -4300 1700
Wire Wire Line
	-3500 1700 -3150 1700
Wire Wire Line
	-3750 1500 -3750 1350
Wire Wire Line
	-3750 1350 -3550 1350
Text Label -3550 1350 0    50   ~ 0
OE3
Text Label -4300 800  2    50   ~ 0
A1
Text Label -3150 800  0    50   ~ 0
OA1
Wire Wire Line
	-2200 800  -2450 800 
Wire Wire Line
	-1650 800  -1300 800 
Wire Wire Line
	-1900 600  -1900 450 
Wire Wire Line
	-1900 450  -1700 450 
Text Label -1700 450  0    50   ~ 0
OE3
Text Label -2450 3500 2    50   ~ 0
A8
Text Label -1300 3500 0    50   ~ 0
OA8
Wire Wire Line
	-2200 1700 -2450 1700
Wire Wire Line
	-1650 1700 -1300 1700
Wire Wire Line
	-1900 1500 -1900 1350
Wire Wire Line
	-1900 1350 -1700 1350
Text Label -1700 1350 0    50   ~ 0
OE3
Text Label -2450 800  2    50   ~ 0
A9
Text Label -1300 800  0    50   ~ 0
OA9
Wire Wire Line
	-4050 2600 -4300 2600
Wire Wire Line
	-3500 2600 -3150 2600
Wire Wire Line
	-3750 2400 -3750 2250
Wire Wire Line
	-3750 2250 -3550 2250
Text Label -3550 2250 0    50   ~ 0
OE3
Text Label -4300 1700 2    50   ~ 0
A2
Text Label -3150 1700 0    50   ~ 0
OA2
Wire Wire Line
	-4050 3500 -4300 3500
Wire Wire Line
	-3500 3500 -3150 3500
Wire Wire Line
	-3750 3300 -3750 3150
Wire Wire Line
	-3750 3150 -3550 3150
Text Label -3550 3150 0    50   ~ 0
OE3
Text Label -4300 2600 2    50   ~ 0
A3
Text Label -3150 2600 0    50   ~ 0
OA3
Wire Wire Line
	-2200 2600 -2450 2600
Wire Wire Line
	-1650 2600 -1300 2600
Wire Wire Line
	-1900 2400 -1900 2250
Wire Wire Line
	-1900 2250 -1700 2250
Text Label -1700 2250 0    50   ~ 0
OE3
Text Label -2450 1700 2    50   ~ 0
A10
Text Label -1300 1700 0    50   ~ 0
OA10
Wire Wire Line
	-2200 3500 -2450 3500
Wire Wire Line
	-1650 3500 -1300 3500
Wire Wire Line
	-1900 3300 -1900 3150
Wire Wire Line
	-1900 3150 -1700 3150
Text Label -1700 3150 0    50   ~ 0
OE3
Text Label -2450 2600 2    50   ~ 0
A11
Text Label -1300 2600 0    50   ~ 0
OA11
$Comp
L 74xGxx:74AHC1G125 U25
U 1 1 5CA8642D
P -3750 3500
F 0 "U25" H -3775 3326 50  0000 C CNN
F 1 "74AHC1G125" H -3775 3235 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -3750 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -3750 3500 50  0001 C CNN
	1    -3750 3500
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U24
U 1 1 5CA86437
P -3750 2600
F 0 "U24" H -3775 2426 50  0000 C CNN
F 1 "74AHC1G125" H -3775 2335 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -3750 2600 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -3750 2600 50  0001 C CNN
	1    -3750 2600
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U32
U 1 1 5CA86441
P -1900 2600
F 0 "U32" H -1925 2426 50  0000 C CNN
F 1 "74AHC1G125" H -1925 2335 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -1900 2600 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -1900 2600 50  0001 C CNN
	1    -1900 2600
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U33
U 1 1 5CA8644B
P -1900 3500
F 0 "U33" H -1925 3326 50  0000 C CNN
F 1 "74AHC1G125" H -1925 3235 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -1900 3500 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -1900 3500 50  0001 C CNN
	1    -1900 3500
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U22
U 1 1 5CA86455
P -3750 800
F 0 "U22" H -3775 626 50  0000 C CNN
F 1 "74AHC1G125" H -3775 535 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -3750 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -3750 800 50  0001 C CNN
	1    -3750 800 
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U23
U 1 1 5CA8645F
P -3750 1700
F 0 "U23" H -3775 1526 50  0000 C CNN
F 1 "74AHC1G125" H -3775 1435 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -3750 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -3750 1700 50  0001 C CNN
	1    -3750 1700
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U31
U 1 1 5CA86469
P -1900 1700
F 0 "U31" H -1925 1526 50  0000 C CNN
F 1 "74AHC1G125" H -1925 1435 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -1900 1700 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -1900 1700 50  0001 C CNN
	1    -1900 1700
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U30
U 1 1 5CA86473
P -1900 800
F 0 "U30" H -1925 626 50  0000 C CNN
F 1 "74AHC1G125" H -1925 535 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -1900 800 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -1900 800 50  0001 C CNN
	1    -1900 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	-4000 550  -4000 450 
Wire Wire Line
	-4000 450  -4200 450 
Text Label -4200 450  2    50   ~ 0
VCC2
Wire Wire Line
	-4000 1450 -4000 1350
Wire Wire Line
	-4000 1350 -4200 1350
Text Label -4200 1350 2    50   ~ 0
VCC2
Wire Wire Line
	-2150 550  -2150 450 
Wire Wire Line
	-2150 450  -2350 450 
Text Label -2350 450  2    50   ~ 0
VCC2
Wire Wire Line
	-2150 1450 -2150 1350
Wire Wire Line
	-2150 1350 -2350 1350
Text Label -2350 1350 2    50   ~ 0
VCC2
Wire Wire Line
	-2150 2350 -2150 2250
Wire Wire Line
	-2150 2250 -2350 2250
Text Label -2350 2250 2    50   ~ 0
VCC2
Wire Wire Line
	-4000 2350 -4000 2250
Wire Wire Line
	-4000 2250 -4200 2250
Text Label -4200 2250 2    50   ~ 0
VCC2
Wire Wire Line
	-2150 3250 -2150 3150
Wire Wire Line
	-2150 3150 -2350 3150
Text Label -2350 3150 2    50   ~ 0
VCC2
Wire Wire Line
	-4000 3250 -4000 3150
Wire Wire Line
	-4000 3150 -4200 3150
Text Label -4200 3150 2    50   ~ 0
VCC2
Text Notes -4300 -250 0    168  ~ 0
ADDR OUT   5v to 3.3v
Wire Wire Line
	-4000 4500 -4250 4500
Wire Wire Line
	-3450 4500 -3100 4500
Wire Wire Line
	-3700 4300 -3700 4150
Wire Wire Line
	-3700 4150 -3500 4150
Text Label -3500 4150 0    50   ~ 0
OE3
Text Label -4300 3500 2    50   ~ 0
A4
Wire Wire Line
	-4000 5400 -4250 5400
Wire Wire Line
	-3450 5400 -3100 5400
Wire Wire Line
	-3700 5200 -3700 5050
Wire Wire Line
	-3700 5050 -3500 5050
Text Label -3500 5050 0    50   ~ 0
OE3
Text Label -4250 5400 2    50   ~ 0
A5
Text Label -3100 5400 0    50   ~ 0
OA5
Wire Wire Line
	-2150 4500 -2400 4500
Wire Wire Line
	-1600 4500 -1250 4500
Wire Wire Line
	-1850 4300 -1850 4150
Wire Wire Line
	-1850 4150 -1650 4150
Text Label -1650 4150 0    50   ~ 0
OE3
Text Label -2400 4500 2    50   ~ 0
A12
Text Label -1250 4500 0    50   ~ 0
OA12
Wire Wire Line
	-2150 5400 -2400 5400
Wire Wire Line
	-1600 5400 -1250 5400
Wire Wire Line
	-1850 5200 -1850 5050
Wire Wire Line
	-1850 5050 -1650 5050
Text Label -1650 5050 0    50   ~ 0
OE3
Text Label -2400 5400 2    50   ~ 0
A13
Text Label -1250 5400 0    50   ~ 0
OA13
Wire Wire Line
	-4000 6300 -4250 6300
Wire Wire Line
	-3450 6300 -3100 6300
Wire Wire Line
	-3700 6100 -3700 5950
Wire Wire Line
	-3700 5950 -3500 5950
Text Label -3500 5950 0    50   ~ 0
OE3
Text Label -4250 6300 2    50   ~ 0
A6
Text Label -3100 6300 0    50   ~ 0
OA6
Wire Wire Line
	-4000 7200 -4250 7200
Wire Wire Line
	-3450 7200 -3100 7200
Wire Wire Line
	-3700 7000 -3700 6850
Wire Wire Line
	-3700 6850 -3500 6850
Text Label -3500 6850 0    50   ~ 0
OE3
Text Label -4250 7200 2    50   ~ 0
A7
Text Label -3100 7200 0    50   ~ 0
OA7
Wire Wire Line
	-2150 6300 -2400 6300
Wire Wire Line
	-1600 6300 -1250 6300
Wire Wire Line
	-1850 6100 -1850 5950
Wire Wire Line
	-1850 5950 -1650 5950
Text Label -1650 5950 0    50   ~ 0
OE3
Text Label -2400 6300 2    50   ~ 0
A14
Text Label -1250 6300 0    50   ~ 0
OA14
Wire Wire Line
	-2150 7200 -2400 7200
Wire Wire Line
	-1600 7200 -1250 7200
Wire Wire Line
	-1850 7000 -1850 6850
Wire Wire Line
	-1850 6850 -1650 6850
Text Label -1650 6850 0    50   ~ 0
OE3
Text Label -2400 7200 2    50   ~ 0
A15
Text Label -1250 7200 0    50   ~ 0
OA15
$Comp
L 74xGxx:74AHC1G125 U29
U 1 1 5CACCDD2
P -3700 7200
F 0 "U29" H -3725 7026 50  0000 C CNN
F 1 "74AHC1G125" H -3725 6935 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -3700 7200 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -3700 7200 50  0001 C CNN
	1    -3700 7200
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U28
U 1 1 5CACCDDC
P -3700 6300
F 0 "U28" H -3725 6126 50  0000 C CNN
F 1 "74AHC1G125" H -3725 6035 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -3700 6300 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -3700 6300 50  0001 C CNN
	1    -3700 6300
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U36
U 1 1 5CACCDE6
P -1850 6300
F 0 "U36" H -1875 6126 50  0000 C CNN
F 1 "74AHC1G125" H -1875 6035 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -1850 6300 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -1850 6300 50  0001 C CNN
	1    -1850 6300
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U37
U 1 1 5CACCDF0
P -1850 7200
F 0 "U37" H -1875 7026 50  0000 C CNN
F 1 "74AHC1G125" H -1875 6935 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -1850 7200 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -1850 7200 50  0001 C CNN
	1    -1850 7200
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U26
U 1 1 5CACCDFA
P -3700 4500
F 0 "U26" H -3725 4326 50  0000 C CNN
F 1 "74AHC1G125" H -3725 4235 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -3700 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -3700 4500 50  0001 C CNN
	1    -3700 4500
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U27
U 1 1 5CACCE04
P -3700 5400
F 0 "U27" H -3725 5226 50  0000 C CNN
F 1 "74AHC1G125" H -3725 5135 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -3700 5400 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -3700 5400 50  0001 C CNN
	1    -3700 5400
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U35
U 1 1 5CACCE0E
P -1850 5400
F 0 "U35" H -1875 5226 50  0000 C CNN
F 1 "74AHC1G125" H -1875 5135 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -1850 5400 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -1850 5400 50  0001 C CNN
	1    -1850 5400
	1    0    0    -1  
$EndComp
$Comp
L 74xGxx:74AHC1G125 U34
U 1 1 5CACCE18
P -1850 4500
F 0 "U34" H -1875 4326 50  0000 C CNN
F 1 "74AHC1G125" H -1875 4235 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H -1850 4500 50  0001 C CNN
F 3 "http://www.ti.com/lit/sg/scyt129e/scyt129e.pdf" H -1850 4500 50  0001 C CNN
	1    -1850 4500
	1    0    0    -1  
$EndComp
Wire Wire Line
	-3950 4250 -3950 4150
Wire Wire Line
	-3950 4150 -4150 4150
Text Label -4150 4150 2    50   ~ 0
VCC2
Wire Wire Line
	-3950 5150 -3950 5050
Wire Wire Line
	-3950 5050 -4150 5050
Text Label -4150 5050 2    50   ~ 0
VCC2
Wire Wire Line
	-2100 4250 -2100 4150
Wire Wire Line
	-2100 4150 -2300 4150
Text Label -2300 4150 2    50   ~ 0
VCC2
Wire Wire Line
	-2100 5150 -2100 5050
Wire Wire Line
	-2100 5050 -2300 5050
Text Label -2300 5050 2    50   ~ 0
VCC2
Wire Wire Line
	-2100 6050 -2100 5950
Wire Wire Line
	-2100 5950 -2300 5950
Text Label -2300 5950 2    50   ~ 0
VCC2
Wire Wire Line
	-3950 6050 -3950 5950
Wire Wire Line
	-3950 5950 -4150 5950
Text Label -4150 5950 2    50   ~ 0
VCC2
Wire Wire Line
	-2100 6950 -2100 6850
Wire Wire Line
	-2100 6850 -2300 6850
Text Label -2300 6850 2    50   ~ 0
VCC2
Wire Wire Line
	-3950 6950 -3950 6850
Wire Wire Line
	-3950 6850 -4150 6850
Text Label -4150 6850 2    50   ~ 0
VCC2
Wire Notes Line
	-650 0    -650 7800
Wire Notes Line
	-650 7800 -5200 7800
Wire Notes Line
	-5200 7800 -5200 0   
Wire Notes Line
	-5200 0    -650 0   
Text Label -3150 3500 0    50   ~ 0
OA4
Wire Wire Line
	1050 2950 750  2950
Text Label 750  2950 0    70   ~ 0
VCC
Wire Wire Line
	1350 2950 1450 2950
Text Label 1450 2950 0    70   ~ 0
GND
$Comp
L Gameboy_Cartridge-eagle-import:C-EUC0805 C2
U 1 0 5CBE7830
P 1150 2950
F 0 "C2" H 1210 2965 59  0000 L BNN
F 1 "20nF" H 1210 2765 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1150 2950 50  0001 C CNN
F 3 "" H 1150 2950 50  0001 C CNN
	1    1150 2950
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1050 3900 750  3900
Text Label 750  3900 2    70   ~ 0
AUDIO_IN
Wire Wire Line
	1350 3900 1450 3900
Text Label 1450 3900 0    70   ~ 0
GND
$Comp
L Gameboy_Cartridge-eagle-import:C-EUC0805 AUDIO1
U 1 0 5CB6784D
P 1150 3900
F 0 "AUDIO1" V 1400 3750 59  0000 L BNN
F 1 "20nF" H 1210 3715 59  0001 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1150 3900 50  0001 C CNN
F 3 "" H 1150 3900 50  0001 C CNN
	1    1150 3900
	0    -1   -1   0   
$EndComp
$Comp
L Regulator_Linear:TLV75509PDBV U38
U 1 1 5CA979EA
P 1800 700
F 0 "U38" H 1800 1042 50  0000 C CNN
F 1 "TLV75509PDBV" H 1800 951 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 1800 1000 50  0001 C CIN
F 3 "http://www.ti.com/lit/ds/symlink/tlv755p.pdf" H 1800 700 50  0001 C CNN
	1    1800 700 
	1    0    0    -1  
$EndComp
Wire Wire Line
	2300 1200 2300 1300
Text Label 2300 1300 3    70   ~ 0
GND
$Comp
L Gameboy_Cartridge-eagle-import:C-EUC0805 C7
U 1 0 5CAA22F0
P 2300 1000
F 0 "C7" H 2360 1015 59  0000 L BNN
F 1 "20nF" H 2360 815 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 2300 1000 50  0001 C CNN
F 3 "" H 2300 1000 50  0001 C CNN
	1    2300 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1100 1150 1100 1250
Text Label 1100 1250 3    70   ~ 0
GND
$Comp
L Gameboy_Cartridge-eagle-import:C-EUC0805 C6
U 1 0 5CAA22FE
P 1100 950
F 0 "C6" H 1160 965 59  0000 L BNN
F 1 "20nF" H 1160 765 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric" H 1100 950 50  0001 C CNN
F 3 "" H 1100 950 50  0001 C CNN
	1    1100 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1500 700  1350 700 
Wire Wire Line
	1350 700  1350 600 
Wire Wire Line
	1500 600  1350 600 
Wire Wire Line
	2100 600  2300 600 
Text Label 2500 600  0    50   ~ 0
VCC2
Wire Wire Line
	2300 900  2300 600 
Connection ~ 2300 600 
Wire Wire Line
	2300 600  2500 600 
Wire Wire Line
	1100 850  1100 600 
Wire Wire Line
	1100 600  1350 600 
Connection ~ 1350 600 
Wire Wire Line
	1100 600  950  600 
Connection ~ 1100 600 
Text Label 950  600  2    50   ~ 0
VCC
Wire Wire Line
	1800 1000 1800 1200
Text Label 1800 1200 3    50   ~ 0
GND
Text Label 5950 1950 0    50   ~ 0
OA2
Text Label 5950 1850 0    50   ~ 0
OA4
$EndSCHEMATC
